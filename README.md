# Joomla 4 Starter Template

## Установка

Рекомендуется использовать редактор [VS Code](https://code.visualstudio.com/download)

1. [Плагин SFTP для VS Code](https://marketplace.visualstudio.com/items?itemName=Natizyskunk.sftp
)
2. Плагин для форматирования кода [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
3. [Git for Windows](https://git-scm.com/download/win)
4. [Node.js](https://nodejs.org/en/download/)

## Подготовка

### .vscode/sftp.json
```json
{
    "name": "website.com",
    "host": "127.0.0.1".
    "protocol": "ftp",
    "port": 21,
    "username": "username",
    "password": "password",
    "remotePath": "/",
    "uploadOnSave": true,
    "useTempFile": false,
    "openSsh": false
}
```

Для форматирования кода используется Prettier с таким конфигом:
### .prettierrc

```json
{
	"arrowParens": "always",
	"bracketSpacing": true,
	"endOfLine": "lf",
	"htmlWhitespaceSensitivity": "css",
	"insertPragma": false,
	"jsxBracketSameLine": false,
	"jsxSingleQuote": false,
	"printWidth": 80,
	"proseWrap": "preserve",
	"quoteProps": "as-needed",
	"requirePragma": false,
	"semi": true,
	"singleQuote": false,
	"tabWidth": 4,
	"trailingComma": "es5",
	"useTabs": true,
	"vueIndentScriptAndStyle": false
}
```
