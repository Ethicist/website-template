<?php
defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Router\Route;

ob_start('compress_html');

function compress_html($compress)
{
	$compress = str_replace("\n", '', $compress);
	$compress = str_replace("\s", '', $compress);
	$compress = str_replace("\r", '', $compress);
	$compress = str_replace("\t", '', $compress);
	$compress = preg_replace('/(?:(?<=\>)|(?<=\/\>))\s+(?=\<\/?)/', '', $compress);

	if (FALSE === strpos($c, '<pre')) {
		$compress = preg_replace('/\s+/', ' ', $compress);
	}

	if (FALSE === strpos($c, 'function goMobile()')) {
		$compress = preg_replace('/\s+/', ' ', $compress);
	}
	$compress = preg_replace('/[\t\r]\s+/', ' ', $compress);
	$compress = preg_replace('/<!(--)([^\[|\|])^(<!-->.*<!--.*-->)/', '', $compress);
	$compress = preg_replace('/\/\*.*?\*\//', '', $compress);

	return preg_replace("#\\s+#ism", " ", $compress);
}

$app = Factory::getApplication();
$db = Factory::getContainer()->get('DatabaseDriver');

$template_path = 'templates/' . $this->template;

$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu     = $app->getMenu()->getActive();

$this->setMetaData('viewport', 'width=device-width, initial-scale=1');
?>

<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
	<jdoc:include type="metas" />
	<meta http-equiv="Permissions-Policy" content="interest-cohort=()" />
	<script src="/<?= $template_path; ?>/js/main.min.js"></script>

	<link rel="apple-touch-icon" sizes="180x180" href="/<?= $template_path; ?>/favicon/apple-touch-icon.png?v=1.1">
	<link rel="icon" type="image/png" sizes="32x32" href="/<?= $template_path; ?>/favicon/favicon-32x32.png?v=1.1">
	<link rel="icon" type="image/png" sizes="194x194" href="/<?= $template_path; ?>/favicon/favicon-194x194.png?v=1.1">
	<link rel="icon" type="image/png" sizes="192x192" href="/<?= $template_path; ?>/favicon/android-chrome-192x192.png?v=1.1">
	<link rel="icon" type="image/png" sizes="16x16" href="/<?= $template_path; ?>/favicon/favicon-16x16.png?v=1.1">
	<link rel="manifest" href="/<?= $template_path; ?>/favicon/site.webmanifest?v=1.1">
	<link rel="mask-icon" href="/<?= $template_path; ?>/favicon/safari-pinned-tab.svg?v=1.1" color="#1f007d">
	<link rel="shortcut icon" href="/<?= $template_path; ?>/favicon/favicon.ico?v=1.1">

	<meta name="apple-mobile-web-app-title" content="<?= $sitename; ?>">
	<meta name="application-name" content="<?= $sitename; ?>">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="msapplication-TileImage" content="/<?= $template_path; ?>/favicon/mstile-144x144.png?v=1.1">
	<meta name="msapplication-config" content="/<?= $template_path; ?>/favicon/browserconfig.xml?v=1.1">
	<meta name="theme-color" content="#ffffff">

	<style>
		<?php
		echo file_get_contents(JPATH_ROOT . '/' . $template_path . '/css/normalize.min.css');
		echo file_get_contents(JPATH_ROOT . '/' . $template_path . '/css/style.css');
		?>
	</style>
</head>

<body>
	<header>
	</header>

	<main>
		<aside>
		</aside>
		<section>
		</section>

		<?php
		switch ($itemid):
			case '101':
				break;
			default:
				echo '<jdoc:include type="message" />';
				echo '<jdoc:include type="component" />';
				break;
		endswitch;
		?>

	</main>

	<footer>
	</footer>

	<jdoc:include type="scripts" />
</body>

<?php
ob_end_flush();
?>