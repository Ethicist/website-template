const log = console.log;
const info = console.info;
const protocol = window.location.protocol;
let dark_ui = false

if (localStorage.getItem('color-theme') === 'dark' ||
	(!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
	document.documentElement.classList.add('dark')
	dark_ui = true
} else {
	document.documentElement.classList.remove('dark')
}

document.documentElement.setAttribute(
	"data-color-scheme",
	dark_ui ? "dark" : "light"
)

const ready = (fn) => {
	if (document.readyState !== 'loading') {
		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}

const is_mobile = () => {
	var match = window.matchMedia || window.msMatchMedia;
	if (match) {
		var mq = match("(pointer:coarse)");
		return mq.matches;
	}
	return false;
}

const show = (el) => {
	el.classList.add('show_alt');
	el.classList.remove('hide_alt');
}

const hide = (el) => {
	el.classList.remove('show_alt');
	el.classList.add('hide_alt');
}

const scroll_top = () => {
	window.scrollTo(
		{
			top: 0,
			left: 0,
			behavior: 'smooth'
		}
	)
}

const is_touch_device = () => {
	return 'ontouchstart' in window || navigator.maxTouchPoints;
}

const change_xlink_href = (selector, path) => {
	document.querySelector(selector).setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', path);
}

const injectCSS = (url) => {
	return new Promise((resolve, reject) => {
		let link = document.createElement('link');
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.onload = () => {
			resolve();
			log('Injected CSS: ' + window.location.protocol + "//" + window.location.host + url);
		};
		link.href = url;

		let headScript = document.querySelector('script');
		headScript.parentNode.insertBefore(link, headScript);
	});
}

const set_cookie = (cname, cvalue, exmins) => {
	let d = new Date();
	d.setTime(d.getTime() + (exmins * 60 * 1000));
	let expires = 'expires=' + d.toGMTString();
	document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

const get_cookie = (cname) => {
	let name = cname + '=';
	let decoded_cookie = decodeURIComponent(document.cookie);
	let ca = decoded_cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}

const get_random_int = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

const delete_cookie = (name) => {
	if (get_cookie(name)) {
		document.cookie = name + "=" +
			";expires=Thu, 01 Jan 1970 00:00:01 GMT";
	}
}

const replace_string = (search_str, replace_str, target_str) => {
	for (let i = 0; i < target_str.length; ++i) {
		if (target_str.substring(i, i + search_str.length) == search_str) {
			target_str = target_str.substring(0, i) + replace_str + target_str.substring(i + search_str.length, target_str.length);
		}
	}
	return target_str;
};

[...document.querySelectorAll('[clickable]')].forEach(
	(item) => {
		item.addEventListener(
			'click',
			(event) => {
				event.preventDefault();
				event.stopPropagation();
				event.stopImmediatePropagation();
				window.open(item.dataset.href, "_self");
			}
		)
	}
)

ready(
	() => {
		let vh = window.innerHeight * 0.01;
		document.documentElement.style.setProperty('--vh', `${vh}px`);

		window.addEventListener(
			'resize',
			() => {
				let vh = window.innerHeight * 0.01;
				document.documentElement.style.setProperty('--vh', `${vh}px`);
			}
		)
	}
)