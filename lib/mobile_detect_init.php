<?php

  if (class_exists('Mobile_Detect')){
    return;
  }

  $detect = false;
  $is_mobile = false;
  $is_tablet = false;
  $is_desktop = false;
  $is_android = false;
  $is_ios = false;

  $library = __DIR__ . '/Mobile_Detect.php';

  if (is_readable($library)) {
    
    include_once($library);

    if (!class_exists('Mobile_Detect')){
      die("Your file $library doesn't have the Mobile_Detect class. "
        . "Make sure that file has this class.");
    }

    $detect = new Mobile_Detect;
    $is_tablet = $detect->isTablet();
    $is_mobile = $detect->isMobile() && !$is_tablet;
    $is_desktop = !$is_tablet && !$is_mobile;
    $is_android = $detect->isAndroidOS();
    $is_ios = $detect->isiOS();
    
  } else {
    die("The $library file can't be included. Does it exist?");
  }

?>